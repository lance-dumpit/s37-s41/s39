const Product = require("../models/Product");
const User = require("../models/User");

//Create a product(Admin only)

module.exports.addProduct = (reqBody, userData) => {

    return User.findById(userData.userId).then(result => {

        if (userData.isAdmin == false) {
            return false
        } else {
            let newProduct = new Product({
                name: reqBody.name,
                description: reqBody.description,
                price: reqBody.price
            })
        
            //Saves the created object to the database
            return newProduct.save().then((product, error) => {
                //Product creation failed
                if(error) {
                    return false
                } else {
                    //Product creation successful
                    return true
                }
            })
        }
        
    });    
}

//Retrieve all active products
module.exports.getAllActive = () => {

    return Product.find({isActive: true}).then(result => {
        return result
    })
}


//Retrieve a single product
module.exports.getProduct = (reqParams) => {

    return Product.findById(reqParams.productId).then(result => {
        return result;
    })
} 

//Update a product information
module.exports.updateProduct = (data) => {
    console.log(data)

    return Product.findById(data.productId).then((result, error) => {

        console.log(result);

        if(data.isAdmin){
            result.name = data.updatedProduct.name
            result.description = data.updatedProduct.description
            result.price = data.updatedProduct.price

            console.log(result)

            return result.save().then((updatedProduct, error) => {

                if(error){
                    return false
                } else {
                    return updatedProduct
                }
            })

        } else {
            return false
        }
    })
}

// Archive a product
module.exports.archiveProduct = (data) => {

    return Product.findById(data.productId).then((result, err) => {

        if(data.payload === true) {
            result.isActive = false;

            return result.save().then((archivedProduct, err) => {

                if(err) {

                    return false;

                } else {

                    return true;
                }
            })

        } else {

            //If user is not Admin
            return false
        }

    })
}