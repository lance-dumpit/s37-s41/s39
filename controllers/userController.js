const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");


//User Registration
module.exports.registerUser = (reqBody) => {
	
		let newUser = new User({
				email: reqBody.email,
				password: bcrypt.hashSync(reqBody.password, 10)
		})

		//Saves the created object to our database
		return newUser.save().then((user, error) => {

			//Registration failed
			if(error){
				return false

			//Registration successful 
			} else {
				return true
			}
		})
}


//User Authentication
module.exports.loginUser = (reqBody) => {

	return User.findOne({email: reqBody.email}).then(result =>{

		if(result == null){
			return false
		} else {

			//compareSync(dataToBeCompared, encryptedData)
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			if(isPasswordCorrect) {
				return { access: auth.createAccessToken(result)}
			} else {
				return false;
			}
		}
	})
}

// Set User as Admin

module.exports.setAsAdmin = (data) => {
	console.log(data)

	return User.findById(data.adminUserId).then((result, error) => {

		if(error) {

			return false
		} else {
			// if user is an admin
			if(result.isAdmin){
				return User.findByIdAndUpdate(data.updateToAdmin, {isAdmin: true}).then((result, error) => {

					if (error){
						return false
					} else {

						return true
					}

				})
			} 

		}
	})
}
		

