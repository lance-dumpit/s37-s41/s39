const express = require("express");
const userController = require("../controllers/userController");
const auth = require("../auth"); 
const router = express.Router();


// Route for User Registration
router.post("/register", (req,res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(
		resultFromController));

});


// Routes for User Authentication
router.post("/login", (req,res) => {

	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
})



// Allows us to export the "router" obejct that will be accessed
module.exports = router

//Set user as Admin

router.put("/:userId/setAsAdmin",auth.verify, (req,res) => {

	const userData = auth.decode(req.headers.authorization)

	const data = {

		updateToAdmin: req.params.userId, //user ID to set as admin
		adminUserId: userData.id //admin user ID that is loggen in 
			
	}

	userController.setAsAdmin(data).then(resultFromController => res.send(resultFromController));
})